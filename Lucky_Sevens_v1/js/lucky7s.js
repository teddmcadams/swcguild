//jslint yelled at me
var alert;
var console;
//dice 
var svnDiceOne;
var svnDiceTwo;
var svnDiceBoth;
//money
var svnStartMoney;
var svnMoney;
var svnHighestMoney;
//rolls
var svnHighestRolls;
var svnTotalRolls;
//game
var svnTryAgain;
var svnClearAll;
var svnPlayGame;

/*
function svnAskBet() {
    'use strict';
    alert('Please enter your starting bet.');
}
function svnLoadStuff() {
    'use strict';
    svnAskBet();
}
*/
function svnToggle() {
    'use strict';
    
}
function svnRollDice() {
    'use strict';
    svnDiceBoth = ((svnDiceOne = (Math.floor(Math.random() * 6 + 1))) + (svnDiceTwo = (Math.floor(Math.random() * 6 + 1))));
}
function svnPlayGame() {
    'use strict';
    svnStartMoney = document.getElementById('svnStartBet').value;
    svnStartMoney = Number(svnStartMoney);
    if (svnStartMoney <= 0 || isNaN(svnStartMoney)) {
        alert('Place your bet! Enter as whole dollars.');
    } else {
        svnMoney = svnStartMoney;
        svnHighestMoney = svnStartMoney;
        svnTotalRolls = 0;
        svnHighestRolls = 0;
        do {    svnRollDice();
                svnTotalRolls = svnTotalRolls + 1;
                if (svnDiceBoth === 7) {
                svnMoney = svnMoney + 4;
                if (svnMoney > svnHighestMoney) {
                    svnHighestMoney = svnMoney;
                    svnHighestRolls = svnTotalRolls;
                }
            } else {
                svnMoney = svnMoney - 1;
            }
            } while (svnMoney >= 1);
        document.getElementById('resultBet').value = svnStartMoney;
        document.getElementById('resultRolls').value = svnTotalRolls;
        document.getElementById('resultHigh').value = svnHighestMoney;
        document.getElementById('resultHighRoll').value = svnHighestRolls;
        document.getElementById('svnStartGame').style.display = 'none';
        document.getElementById('svnResults').style.display = 'block';
    }
}
function svnClearAll() {
    'use strict';
    svnMoney = 0;
    svnStartMoney = 0;
    svnHighestMoney = 0;
    svnHighestRolls = 0;
    svnTotalRolls = 0;
}
function svnTryAgain() {
    'use strict';
    svnClearAll();
    document.getElementById('svnStartBet').value = null;
    document.getElementById('resultBet').value = null;
    document.getElementById('resultRolls').value = null;
    document.getElementById('resultHigh').value = null;
    document.getElementById('resultHighRoll').value = null;
    document.getElementById('svnStartGame').style.display = 'block';
    document.getElementById('svnResults').style.display = 'none';
}