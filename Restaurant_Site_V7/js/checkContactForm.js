function oneBoxChecked() {
    'use strict';
    var dateBoxes = document.getElementsByName('_days'),
        i;
    for (i = 0; i < dateBoxes.length; i += 1) {
        if (dateBoxes[i].checked) {
            return true;
        }
    }
    return false;
}
function checkBeforeSubmit() {
    'use strict';
    var submitReady;
    submitReady = true;
// Check if name is empty
    if (document.getElementById('input_Name').value === "") {
        document.getElementById('input_Name').classList.add('alert');
        document.getElementById('input_Name').classList.add('alert-danger');
        submitReady = false;
    } else {
        document.getElementById('input_Name').classList.remove('alert');
        document.getElementById('input_Name').classList.remove('alert-danger');
    }
// Check if email and phone are both empty
    if (document.getElementById('input_Email').value === "" && document.getElementById('input_Phone').value === "") {
        submitReady = false;
        // Check if phone is empty
        if (document.getElementById('input_Phone').value === "") {
            document.getElementById('input_Phone').classList.add('alert');
            document.getElementById('input_Phone').classList.add('alert-danger');
        } else {
            document.getElementById('input_Phone').classList.remove('alert');
            document.getElementById('input_Phone').classList.remove('alert-danger');
        }
        // Check if email is empty
        if (document.getElementById('input_Email').value === "") {
            document.getElementById('input_Email').classList.add('alert');
            document.getElementById('input_Email').classList.add('alert-danger');
        } else {
            document.getElementById('input_Email').classList.remove('alert');
            document.getElementById('input_Email').classList.remove('alert-danger');
        }
    }
// Check if additional info is filled in when Other is selected on dropdown.
    if (document.getElementById('input_Reason').value === "OTHER" && document.getElementById('input_Moreinfo').value === "") {
        document.getElementById('input_Moreinfo').classList.add('alert');
        document.getElementById('input_Moreinfo').classList.add('alert-danger');
        submitReady = false;
    } else {
        document.getElementById('input_Moreinfo').classList.remove('alert');
        document.getElementById('input_Moreinfo').classList.remove('alert-danger');
    }
// Check if any checkbox is ticked    
    if (oneBoxChecked() === false) {
        document.getElementById('input_days').classList.add('alert');
        document.getElementById('input_days').classList.add('alert-danger');
        submitReady = false;
    } else {
        document.getElementById('input_days').classList.remove('alert');
        document.getElementById('input_days').classList.remove('alert-danger');
    }
// Submit if required is filled out
    if (submitReady === false) {
        document.getElementById('reqPrompt').style.display = 'block';
    } else {
        document.getElementById('contact_Form').submit();
        document.getElementById('reqPrompt').style.display = 'none';
        document.getElementById('myContactForm').style.display = 'none';
        document.getElementById('thank_you').style.display = 'block';
    }
}
    
